# Paths

## config
export DEFAULT_PATH = ${CONFIG_PATH}/default
export PARAMS_PATH = ${CONFIG_PATH}/params

## data
export INTERIM_PATH = ${DATA_PATH}/interim
export PROCESSED_PATH = ${DATA_PATH}/processed
export SOURCE_PATH = ${DATA_PATH}/source

## doc
export REFERENCES_PATH = ${DOC_PATH}/references

## src
export DOCOPT_PATH = ${SRC_PATH}/docopt
export PYTHON_PATH = ${SRC_PATH}/python

### INTERIM
export INTERIM_EDGES_PATH = ${INTERIM_PATH}/edges

### PROCESSED
export PROCESSED_PARAMS_PATH = ${PROCESSED_PATH}/params
export PROCESSED_PRIVATE_PATH = ${PROCESSED_PATH}/private

### SOURCE
export SOURCE_PRIVATE_PATH = ${SOURCE_PATH}/private

### R
export FUNCTIONS_PATH = ${R_PATH}/functions
