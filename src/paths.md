# paths

path_r_python = function(x, y = NULL) { 
  if (!is.null(y)) { 
    if (language == "r") { 
      path = file.path(x, y)
    }
    if (language == "python") {
      path = os.pathjoin(x, y)
    }
  return(path)
  }
  if (is.null(y)) { 
    if (language == "r") { 
      path = file.path(x)
    }
    if (language == "python") {
      path = os.pathjoin(x)
    }
    return(path)
  }
}

## config
config = path_r_python("../config")

### default

config_default = path_r_python(config, "default")

#### edges  

config_default_edges = path_r_python(config_default, "edges_default.yaml")

### params

config_params = path_r_python(config, "params")

#### edges  

config_params_edges = path_r_python(config_params, "edges_params.yaml")

## data

data = path_r_python("../data")

### source

data_source = path_r_python(data, "source")

### interim

data_interim = path_r_python(data, "interim")

#### edges

data_interim_edges = path_r_python(data_interim, "edges")

### processed

data_processed = path_r_python(data, "processed")

#### params 

data_processed_params = path_r_python(data_processed, "params")

##### edges 

data_processed_params_edges = path_r_python(data_processed_params, "edges_params.yaml")

## src

### docopt

#### edges
docopt_get_edges = "docopt/get_edges.txt"
