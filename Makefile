include config.mk

.PHONY: get-edges
.PRECIOUS: %.tsv %.zip %.json %.gz

get-edges:
	cd src && Rscript R/get_edges.R